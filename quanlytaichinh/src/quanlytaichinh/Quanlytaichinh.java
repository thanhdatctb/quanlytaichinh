/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quanlytaichinh;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import quanlytaichinh.obj.ChiPhi;
import quanlytaichinh.obj.GiangVien;
import quanlytaichinh.obj.GiaoDich;
import quanlytaichinh.obj.LaoDong;
import quanlytaichinh.obj.SinhVien;
import quanlytaichinh.obj.Truong;

/**
 *
 * @author cx
 */
public class Quanlytaichinh {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        Scanner scan = new Scanner(System.in);
        Truong truong = new Truong(0, "Truong", 0);

        SinhVien SV[] = new SinhVien[3];
        SV[0] = new SinhVien(1, "sinh vien 1", 13, 1600);
        SV[1] = new SinhVien(2, "sinh vien 2", 15, 2200);
        SV[2] = new SinhVien(3, "sinh vien 3", 17, 2400);
        List<LaoDong> laoDongs = Arrays.asList(
                new LaoDong(1, "Lao dong 1", 1987, "Hanh Chinh", 100, 25, 50),
                new LaoDong(2, "Lao dong 2", 1987, "Hanh Chinh", 200, 25, 50)
        );
        ChiPhi CP[] = new ChiPhi[2];
        CP[0] = new ChiPhi(1, "chi phi 1", 2, 40, "thoi gian 1", 1);
        CP[1] = new ChiPhi(2, "chi phi 2", 5, 90, "thoi gian 2", 3);

        while (true) {
            System.out.print("---------------------------------------------\n");
            System.out.print("1 - Thu hoc phi vao tai khoan\n");
            System.out.print("2 - Chi tra chi phi luong cho nguoi lao dong\n");
            System.out.print("3 - Chi tra chi phi van hanh\n");
            System.out.print("4 - Kiem tra thong tin tai khoan\n");
            System.out.print("5 - Tinh tong chi phi phai tra hien tai\n");
            System.out.print("6 - Thoat\n");
            System.out.print(">> ");
            try {
                int choice = scan.nextInt();
                switch (choice) {
                    case 1:
                        for (int i = 0; i < SV.length; i++) {
                            truong.credit(SV[i].TinhHocPhi());
                            System.out.println("Da thu hoc phi cua sinh vien: " + SV[i].getTenSV());
                        }
                        break;
                    case 2: {
                        for(LaoDong a: laoDongs){
                            truong.debit(a.TinhLuong());
                            System.out.println("Đã tra luong cho nhan vien: " + a.getHoTen());
                        }
                        break;
                    }
                    case 3:
                        for (int i = 0; i < CP.length; i++) {
                            if (truong.getBalance() > CP[i].getCost()) {
                                GiaoDich gd = new GiaoDich(1, truong, truong, i, LocalDate.now(), choice);
                                truong.debit(CP[i].getCost());
                                System.out.println("Da chi tra chi phi: " + CP[i].getTenCP());
                            } else {
                                System.out.println("so du khong du de tra chi phi: " + CP[i].getTenCP());
                            }
                        }
                        break;

                    case 4:
                        System.out.println("thong tin tai khoan: " + truong.getBalance());
                        break;
                    case 5:
                        int cost = 0;
                        for(LaoDong ld : laoDongs){
                            cost+=ld.TinhLuong();
                        }
                        for(ChiPhi cp : CP){
                            cost += cp.getCost();
                        }
                        System.out.println("Tổng chi phí: "+cost);
                        break;
                    case 6:
                        System.out.print("Thoat chuong trinh\n");
                        System.exit(0);

                    default:
                        System.out.print("Lenh khong xac dinh\n");
                        break;
                }
            } catch (Exception e) {
                System.out.print("error\n");
                System.exit(0);
            }

        }
    }

}
