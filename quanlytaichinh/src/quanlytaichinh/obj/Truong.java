/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quanlytaichinh.obj;

/**
 *
 * @author cx
 */
public class Truong implements Account{
    int MaTruong;
    String TenTruong;
    int Balance;

    public Truong() {
    }

    public Truong(int MaTruong, String TenTruong, int Balance) {
        this.MaTruong = MaTruong;
        this.TenTruong = TenTruong;
        this.Balance = Balance;
    }

    public int getMaTruong() {
        return MaTruong;
    }

    public void setMaTruong(int MaTruong) {
        this.MaTruong = MaTruong;
    }

    public String getTenTruong() {
        return TenTruong;
    }

    public void setTenTruong(String TenTruong) {
        this.TenTruong = TenTruong;
    }

    public void setSoDu(int balance) {
        this.Balance = balance;
    }

    @Override
    public int getBalance() {
        return this.Balance;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void debit(int amount) {
        if(this.Balance < amount)
        {
            System.out.print("so du khong du\n");
        }
        else
            this.Balance -= amount;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void credit(int amount) {
        this.Balance += amount;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    
}
