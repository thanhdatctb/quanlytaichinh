/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quanlytaichinh.obj;

import java.sql.Time;
import java.time.LocalDate;

/**
 *
 * @author cx
 */
public class GiaoDich {
    int MaGD;
    Account TaiKhoanNguon;
    Account TaiKhoanDich;
    int SoTien;
    LocalDate ThoiGian;
    int KiChiPhi;

    public GiaoDich() {
    }

    public GiaoDich(int MaGD, Account TaiKhoanNguon, Account TaiKhoanDich, int SoTien, LocalDate ThoiGian, int KiChiPhi) {
        this.MaGD = MaGD;
        this.TaiKhoanNguon = TaiKhoanNguon;
        this.TaiKhoanDich = TaiKhoanDich;
        this.SoTien = SoTien;
        this.ThoiGian = ThoiGian;
        this.KiChiPhi = KiChiPhi;
    }

    public int getMaGD() {
        return MaGD;
    }

    public void setMaGD(int MaGD) {
        this.MaGD = MaGD;
    }

    public Account getTaiKhoanNguon() {
        return TaiKhoanNguon;
    }

    public void setTaiKhoanNguon(Account TaiKhoanNguon) {
        this.TaiKhoanNguon = TaiKhoanNguon;
    }

    public Account getTaiKhoanDich() {
        return TaiKhoanDich;
    }

    public void setTaiKhoanDich(Account TaiKhoanDich) {
        this.TaiKhoanDich = TaiKhoanDich;
    }

    public int getSoTien() {
        return SoTien;
    }

    public void setSoTien(int SoTien) {
        this.SoTien = SoTien;
    }

    public LocalDate getThoiGian() {
        return ThoiGian;
    }

    public void setThoiGian(LocalDate ThoiGian) {
        this.ThoiGian = ThoiGian;
    }

    public int getKiChiPhi() {
        return KiChiPhi;
    }

    public void setKiChiPhi(int KiChiPhi) {
        this.KiChiPhi = KiChiPhi;
    }
    
    
    
}
