/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quanlytaichinh.obj;

/**
 *
 * @author cx
 */
public abstract class Staff {
    
    private String name;
    private int yob;    
    private int SoDu;
    public abstract int getSalary();

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYob() {
        return yob;
    }

    public void setYob(int yob) {
        this.yob = yob;
    }

    public int getSoDu() {
        return SoDu;
    }

    public void setSoDu(int SoDu) {
        this.SoDu = SoDu;
    }
    
}
