/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quanlytaichinh.obj;

/**
 *
 * @author cx
 */
public class GiangVien extends Staff implements Account{
    private int MaGV;
    private String HoTen;
    private int NamSinh;
    private String Khoa;
    private String TrinhDo;
    
    private int PhuCap;
    private int SoTietTrongThang;
    private int HeSoLuong;
    private int NamBatDauLamViec;

    public GiangVien() {
    }
    public double TinhLuong(){
        
        return this.HeSoLuong*1000 + this.getPhuCap() + this.SoTietTrongThang* 45 ;
    }
    public GiangVien(int MaGV, String HoTen, int NamSinh, String Khoa, String TrinhDo, int PhuCap, int SoTietTrongThang, int HeSoLuong, int NamBatDauLamViec) {
        this.MaGV = MaGV;
        this.HoTen = HoTen;
        this.NamSinh = NamSinh;
        this.Khoa = Khoa;
        this.TrinhDo = TrinhDo;
        this.PhuCap = PhuCap;
        this.SoTietTrongThang = SoTietTrongThang;
        this.HeSoLuong = HeSoLuong;
        this.NamBatDauLamViec = NamBatDauLamViec;
    }

    public String getMaGV() {
        return "GV" + MaGV;
    }

    public void setMaGV(int MaGV) {
        this.MaGV = MaGV;
    }

    public String getHoTen() {
        return HoTen;
    }

    public void setHoTen(String HoTen) {
        this.HoTen = HoTen;
    }

    public int getNamSinh() {
        return NamSinh;
    }

    public void setNamSinh(int NamSinh) {
        this.NamSinh = NamSinh;
    }

    public String getKhoa() {
        return Khoa;
    }

    public void setKhoa(String Khoa) {
        this.Khoa = Khoa;
    }

    public String getTrinhDo() {
        return TrinhDo;
    }

    public void setTrinhDo(String TrinhDo) {
        this.TrinhDo = TrinhDo;
    }

    public int getPhuCap() {
        return PhuCap;
    }

    public void setPhuCap(int PhuCap) {
        this.PhuCap = PhuCap;
    }

    public int getSoTietTrongThang() {
        return SoTietTrongThang;
    }

    public void setSoTietTrongThang(int SoTietTrongThang) {
        this.SoTietTrongThang = SoTietTrongThang;
    }

    public int getHeSoLuong() {
        return HeSoLuong;
    }

    public void setHeSoLuong(int HeSoLuong) {
        this.HeSoLuong = HeSoLuong;
    }

    public int getNamBatDauLamViec() {
        return NamBatDauLamViec;
    }

    public void setNamBatDauLamViec(int NamBatDauLamViec) {
        this.NamBatDauLamViec = NamBatDauLamViec;
    }

    @Override
    public void setYob(int yob) {
        super.setYob(yob); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getYob() {
        return super.getYob(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setName(String name) {
        super.setName(name); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getName() {
        return super.getName(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getSalary() {
        return (int)TinhLuong();
//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getBalance() {
        return this.getSoDu();
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void debit(int amount) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        
    }

    @Override
    public void credit(int amount) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
