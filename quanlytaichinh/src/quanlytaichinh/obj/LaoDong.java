/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quanlytaichinh.obj;

/**
 *
 * @author cx
 */
public class LaoDong extends Staff implements Account{
    int MaLD;
    String HoTen;
    int NamSinh;
    String PhongBan;
    int DonGiaNgayCong;
    int SoNgayCong;
    int SoDuTaiKhoan;

    public LaoDong() {
    }

    public LaoDong(int MaLD, String HoTen, int NamSinh, String PhongBan, int DonGiaNgayCong, int SoNgayCong, int SoDuTaiKhoan) {
        this.MaLD = MaLD;
        this.HoTen = HoTen;
        this.NamSinh = NamSinh;
        this.PhongBan = PhongBan;
        this.DonGiaNgayCong = DonGiaNgayCong;
        this.SoNgayCong = SoNgayCong;
        this.SoDuTaiKhoan = SoDuTaiKhoan;
    }

    public String getMaLD() {
        return "LD" + MaLD;
    }

    public void setMaLD(int MaLD) {
        this.MaLD = MaLD;
    }

    public String getHoTen() {
        return HoTen;
    }

    public void setHoTen(String HoTen) {
        this.HoTen = HoTen;
    }

    public int getNamSinh() {
        return NamSinh;
    }

    public void setNamSinh(int NamSinh) {
        this.NamSinh = NamSinh;
    }

    public String getPhongBan() {
        return PhongBan;
    }

    public void setPhongBan(String PhongBan) {
        this.PhongBan = PhongBan;
    }

    public int getDonGiaNgayCong() {
        return DonGiaNgayCong;
    }

    public void setDonGiaNgayCong(int DonGiaNgayCong) {
        this.DonGiaNgayCong = DonGiaNgayCong;
    }

    public int getSoNgayCong() {
        return SoNgayCong;
    }

    public void setSoNgayCong(int SoNgayCong) {
        this.SoNgayCong = SoNgayCong;
    }

    public int getSoDuTaiKhoan() {
        return SoDuTaiKhoan;
    }

    public void setSoDuTaiKhoan(int SoDuTaiKhoan) {
        this.SoDuTaiKhoan = SoDuTaiKhoan;
    }

    public int TinhLuong()
    {
        
        return this.DonGiaNgayCong * this.SoNgayCong;
    }

    @Override
    public int getBalance() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void debit(int amount) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void credit(int amount) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getSalary() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
