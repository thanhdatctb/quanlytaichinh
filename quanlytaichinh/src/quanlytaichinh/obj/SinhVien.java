/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quanlytaichinh.obj;

/**
 *
 * @author cx
 */
public class SinhVien {
    int MaSV;
    String TenSV;
    int SoTinChiDangKy;
    int DonGiaTinChi;

    public SinhVien() {
    }

    public SinhVien(int MaSV, String TenSV, int SoTinChiDangKy, int DonGiaTinChi) {
        this.MaSV = MaSV;
        this.TenSV = TenSV;
        this.SoTinChiDangKy = SoTinChiDangKy;
        this.DonGiaTinChi = DonGiaTinChi;
    }

    public String getMaSV() {
        return "SV" + MaSV;
    }

    public void setMaSV(int MaSV) {
        this.MaSV = MaSV;
    }

    public String getTenSV() {
        return TenSV;
    }

    public void setTenSV(String TenSV) {
        this.TenSV = TenSV;
    }

    public int getSoTinChiDangKy() {
        return SoTinChiDangKy;
    }

    public void setSoTinChiDangKy(int SoTinChiDangKy) {
        this.SoTinChiDangKy = SoTinChiDangKy;
    }

    public int getDonGiaTinChi() {
        return DonGiaTinChi;
    }

    public void setDonGiaTinChi(int DonGiaTinChi) {
        this.DonGiaTinChi = DonGiaTinChi;
    }
    
    public int TinhHocPhi()
    {
        return this.DonGiaTinChi * this.SoTinChiDangKy;
    }
    
}
