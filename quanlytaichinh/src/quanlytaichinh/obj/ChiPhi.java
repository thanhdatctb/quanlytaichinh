/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quanlytaichinh.obj;

/**
 *
 * @author cx
 */
public class ChiPhi implements Payable{
    int MaCP;
    String TenCP;
    int SoLuong;
    int DonGia;
    String ThoiGian;
    int KyChiPhi;

    public ChiPhi() {
    }

    public ChiPhi(int MaCP, String TenCP, int SoLuong, int DonGia, String ThoiGian, int KyChiPhi) {
        this.MaCP = MaCP;
        this.TenCP = TenCP;
        this.SoLuong = SoLuong;
        this.DonGia = DonGia;
        this.ThoiGian = ThoiGian;
        this.KyChiPhi = KyChiPhi;
    }

    public String getMaCP() {
        return  "CP" + MaCP;
    }

    public void setMaCP(int MaCP) {
        this.MaCP = MaCP;
    }

    public String getTenCP() {
        return TenCP;
    }

    public void setTenCP(String TenCP) {
        this.TenCP = TenCP;
    }

    public int getSoLuong() {
        return SoLuong;
    }

    public void setSoLuong(int SoLuong) {
        this.SoLuong = SoLuong;
    }

    public int getDonGia() {
        return DonGia;
    }

    public void setDonGia(int DonGia) {
        this.DonGia = DonGia;
    }

    public String getThoiGian() {
        return ThoiGian;
    }

    public void setThoiGian(String ThoiGian) {
        this.ThoiGian = ThoiGian;
    }

    public int getKyChiPhi() {
        return KyChiPhi;
    }

    public void setKyChiPhi(int KyChiPhi) {
        this.KyChiPhi = KyChiPhi;
    }

    

    @Override
    public int getCost() {
        return this.DonGia * this.SoLuong;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
