/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quanlytaichinh.obj;

/**
 *
 * @author cx
 */
public class NhanVien {
    int MaNV;
    String HoTen;
    int NamSinh;
    String PhongBan;
    int SoNgayCong;
    int HeSoLuong;
    int PhuCap;
    ChucVu ChucVu;

    public NhanVien() {
    }
    public double TinhLuong(){
        
        return this.HeSoLuong+500+this.getPhuCap() + this.SoNgayCong*30;
    }
    public NhanVien(int MaNV, String HoTen, int NamSinh, String PhongBan, int SoNgayCong, int HeSoLuong, int PhuCap, ChucVu ChucVu) {
        this.MaNV = MaNV;
        this.HoTen = HoTen;
        this.NamSinh = NamSinh;
        this.PhongBan = PhongBan;
        this.SoNgayCong = SoNgayCong;
        this.HeSoLuong = HeSoLuong;
        this.PhuCap = PhuCap;
        this.ChucVu = ChucVu;
    }

    public String getMaNV() {
        return  "NV" + MaNV;
    }

    public void setMaNV(int MaNV) {
        this.MaNV = MaNV;
    }

    public String getHoTen() {
        return HoTen;
    }

    public void setHoTen(String HoTen) {
        this.HoTen = HoTen;
    }

    public int getNamSinh() {
        return NamSinh;
    }

    public void setNamSinh(int NamSinh) {
        this.NamSinh = NamSinh;
    }

    public String getPhongBan() {
        return PhongBan;
    }

    public void setPhongBan(String PhongBan) {
        this.PhongBan = PhongBan;
    }

    public int getSoNgayCong() {
        return SoNgayCong;
    }

    public void setSoNgayCong(int SoNgayCong) {
        this.SoNgayCong = SoNgayCong;
    }

    public int getHeSoLuong() {
        return HeSoLuong;
    }

    public void setHeSoLuong(int HeSoLuong) {
        this.HeSoLuong = HeSoLuong;
    }

    public int getPhuCap() {
        if(this.ChucVu == ChucVu.Cu_nhan)
            return 300;
        if(this.ChucVu == ChucVu.Thac_si)
            return 900;
        if(this.ChucVu == ChucVu.Tien_si)
            return 2000;
        if(this.ChucVu == ChucVu.Truong_phong)
            return 1000;
        if(this.ChucVu == ChucVu.Pho_Phong)
            return 600;
        if(this.ChucVu == ChucVu.Nhan_Vien)
            return 400;
        return 0;
    }

    public void setPhuCap(int PhuCap) {
        this.PhuCap = PhuCap;
    }

    public ChucVu getChucVu() {
        return ChucVu;
    }

    public void setChucVu(ChucVu ChucVu) {
        this.ChucVu = ChucVu;
    }
    
}
